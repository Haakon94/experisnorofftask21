﻿namespace Task19Noroff {
    partial class Task19Form {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.InsertBtn = new System.Windows.Forms.Button();
            this.DeleteBtn = new System.Windows.Forms.Button();
            this.tableGW = new System.Windows.Forms.DataGridView();
            this.txtNameInput = new System.Windows.Forms.TextBox();
            this.SupervisorCb = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.JSONtxtBox = new System.Windows.Forms.TextBox();
            this.SerializeBtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.tableGW)).BeginInit();
            this.SuspendLayout();
            // 
            // InsertBtn
            // 
            this.InsertBtn.Location = new System.Drawing.Point(6, 145);
            this.InsertBtn.Name = "InsertBtn";
            this.InsertBtn.Size = new System.Drawing.Size(102, 36);
            this.InsertBtn.TabIndex = 0;
            this.InsertBtn.Text = "Insert";
            this.InsertBtn.UseVisualStyleBackColor = true;
            this.InsertBtn.Click += new System.EventHandler(this.InsertBtn_Click);
            // 
            // DeleteBtn
            // 
            this.DeleteBtn.Location = new System.Drawing.Point(137, 146);
            this.DeleteBtn.Name = "DeleteBtn";
            this.DeleteBtn.Size = new System.Drawing.Size(98, 35);
            this.DeleteBtn.TabIndex = 2;
            this.DeleteBtn.Text = "Delete";
            this.DeleteBtn.UseVisualStyleBackColor = true;
            this.DeleteBtn.Click += new System.EventHandler(this.DeleteBtn_Click);
            // 
            // tableGW
            // 
            this.tableGW.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableGW.Location = new System.Drawing.Point(263, 12);
            this.tableGW.Name = "tableGW";
            this.tableGW.RowHeadersWidth = 51;
            this.tableGW.RowTemplate.Height = 24;
            this.tableGW.Size = new System.Drawing.Size(615, 257);
            this.tableGW.TabIndex = 4;
            // 
            // txtNameInput
            // 
            this.txtNameInput.Location = new System.Drawing.Point(6, 32);
            this.txtNameInput.Name = "txtNameInput";
            this.txtNameInput.Size = new System.Drawing.Size(229, 22);
            this.txtNameInput.TabIndex = 5;
            // 
            // SupervisorCb
            // 
            this.SupervisorCb.FormattingEnabled = true;
            this.SupervisorCb.Location = new System.Drawing.Point(6, 105);
            this.SupervisorCb.Name = "SupervisorCb";
            this.SupervisorCb.Size = new System.Drawing.Size(229, 24);
            this.SupervisorCb.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 17);
            this.label1.TabIndex = 8;
            this.label1.Text = "Insert name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(138, 17);
            this.label2.TabIndex = 9;
            this.label2.Text = "Choose a supervisor";
            // 
            // JSONtxtBox
            // 
            this.JSONtxtBox.Location = new System.Drawing.Point(263, 317);
            this.JSONtxtBox.Multiline = true;
            this.JSONtxtBox.Name = "JSONtxtBox";
            this.JSONtxtBox.Size = new System.Drawing.Size(615, 232);
            this.JSONtxtBox.TabIndex = 10;
            // 
            // SerializeBtn
            // 
            this.SerializeBtn.Location = new System.Drawing.Point(89, 317);
            this.SerializeBtn.Name = "SerializeBtn";
            this.SerializeBtn.Size = new System.Drawing.Size(157, 45);
            this.SerializeBtn.TabIndex = 11;
            this.SerializeBtn.Text = "Serialize into JSON";
            this.SerializeBtn.UseVisualStyleBackColor = true;
            this.SerializeBtn.Click += new System.EventHandler(this.SerializeBtn_Click);
            // 
            // Task19Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(934, 649);
            this.Controls.Add(this.SerializeBtn);
            this.Controls.Add(this.JSONtxtBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.SupervisorCb);
            this.Controls.Add(this.txtNameInput);
            this.Controls.Add(this.tableGW);
            this.Controls.Add(this.DeleteBtn);
            this.Controls.Add(this.InsertBtn);
            this.Name = "Task19Form";
            this.Text = "Task 19 and 20";
            this.Load += new System.EventHandler(this.Task19Form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tableGW)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button InsertBtn;
        private System.Windows.Forms.Button DeleteBtn;
        private System.Windows.Forms.DataGridView tableGW;
        private System.Windows.Forms.TextBox txtNameInput;
        private System.Windows.Forms.ComboBox SupervisorCb;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox JSONtxtBox;
        private System.Windows.Forms.Button SerializeBtn;
    }
}

