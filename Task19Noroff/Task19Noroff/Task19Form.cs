﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Task19Noroff.Model;

namespace Task19Noroff {
    public partial class Task19Form : Form {

        private Task19NoroffDBContext Task19NoroffContext;
        public Task19Form() {
            InitializeComponent();
            Task19NoroffContext = new Task19NoroffDBContext();

            List<Supervisor> supervisors = Task19NoroffContext.Supervisors.ToList();
            // Populate list to combobox
            foreach (Supervisor item in supervisors) {
                SupervisorCb.Items.Add(item);
            }
        }
        private void Task19Form_Load(object sender, EventArgs e) {
            // Load in the student table
            LoadStudentTable();
        }

        // Method for inserting a student when insert button is clicked
        private void InsertBtn_Click(object sender, EventArgs e) {
            try {
                //new object for EF to use in a DBSet
                Student obj = new Model.Student {
                    Name = txtNameInput.Text,
                    SupervisorId = ((Supervisor)SupervisorCb.SelectedItem).Id

                };
                Task19NoroffContext.Students.Add(obj);
                Task19NoroffContext.SaveChanges();

                // Refresh student table in Data Grid View
                LoadStudentTable();

            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }

        }

        // Method for loading in the student table to data grid view
        public void LoadStudentTable() {
            try {
                BindingSource myBindingSource = new BindingSource();
                tableGW.DataSource = myBindingSource;

                myBindingSource.DataSource = Task19NoroffContext.Students.ToList();
                tableGW.Refresh();

            } catch(Exception ex) {
                Console.WriteLine(ex.Message);
            }
        }

        // Method for deleting when delete button is clicked
        private void DeleteBtn_Click(object sender, EventArgs e) {
            try {
                // Check that user has selected a row for deleting
                if (tableGW.SelectedRows.Count != 0) {
                    int id = Convert.ToInt32(tableGW.SelectedRows[0].Cells[0].Value);
                    Student tempObj = Task19NoroffContext.Students.Find(id);
                    Task19NoroffContext.Students.Remove(tempObj);
                    Task19NoroffContext.SaveChanges();

                    // // Refresh student table in Data Grid View
                    LoadStudentTable();

                } else {
                    MessageBox.Show("Please select a student to delete");
                }     
            } catch(Exception ex) {
                Console.WriteLine(ex.Message); 
            }
        }

        // When serialize to JSON btn is clicked, makes a list of supervisors and convert to JSON and add to textbox
        private void SerializeBtn_Click(object sender, EventArgs e) {
            List<Supervisor> supervisors = Task19NoroffContext.Supervisors.ToList();
            string JSONString = JsonConvert.SerializeObject(supervisors);
            JSONtxtBox.Text = JSONString;
        }
    }
}
